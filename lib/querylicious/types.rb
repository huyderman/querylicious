# frozen_string_literal: true

require 'dry-types'

module Querylicious
  # Types for Querylicious
  module Types
    include Dry.Types(default: :strict)
    Coercible::Symbol = Symbol.constructor { |sym| String(sym).to_sym }
  end
end
